<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, "frontend/Certification/FrontendProfessional", __DIR__);