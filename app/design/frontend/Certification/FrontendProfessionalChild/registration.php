<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, "frontend/Certification/FrontendProfessionalChild", __DIR__);